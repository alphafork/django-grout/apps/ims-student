from django.contrib.auth.models import User
from django.db import models
from ims_base.models import AbstractBaseDetail, AbstractLog


class Student(AbstractLog):
    user = models.OneToOneField(
        User, primary_key=True, on_delete=models.CASCADE, related_name="student"
    )
    student_no = models.CharField(max_length=255, null=True, blank=True, unique=True)

    def __str__(self):
        return f"{self.user.get_full_name()} ({self.student_no})"


class Relationship(AbstractBaseDetail):
    pass


class StudentGuardian(AbstractLog):
    student = models.OneToOneField(Student, primary_key=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=True, blank=True)
    relationship = models.ForeignKey(
        Relationship, on_delete=models.CASCADE, null=True, blank=True
    )
    phone_1 = models.CharField(max_length=12, null=True, blank=True)
    phone_2 = models.CharField(max_length=12, null=True, blank=True)

    def __str__(self):
        return self.student.__str__()


class Employment(AbstractLog):
    student = models.OneToOneField(Student, primary_key=True, on_delete=models.CASCADE)
    is_job_needed = models.BooleanField(null=True, blank=True)
    is_part_time = models.BooleanField(null=True, blank=True)
    is_technical = models.BooleanField(null=True, blank=True)
    resume = models.FileField(upload_to="resume/%Y/%m/%d/", null=True, blank=True)

    def __str__(self):
        return self.student.__str__()


class Placement(AbstractLog):
    employer = models.CharField(max_length=255)
    designation = models.CharField(max_length=255)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    annual_salary_package = models.PositiveIntegerField(null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return self.designation
