from dj_rest_auth.registration.views import RegisterView
from ims_base.apis import BaseAPIView, BaseAPIViewSet
from ims_user.apis import BaseUserAPI, UserFilter
from rest_framework.views import Response

from ims_student.models import Student
from ims_student.serializers import (
    StudentProfileSerializer,
    StudentRegistrationSerializer,
)

from .services import get_profile_status


class StudentInfoAPI(BaseUserAPI):
    user_lookup_field = "student"


class StudentGuardianAPI(StudentInfoAPI):
    pass


class EmploymentAPI(StudentInfoAPI):
    pass


class AcademicAPI(StudentInfoAPI):
    pass


class GetStudentProfileStatusAPI(BaseAPIView):
    def get(self, request, format=None):
        student_profile_status = get_profile_status(request.user)
        return Response(student_profile_status)


class StudentRegistrationAPI(RegisterView):
    serializer_class = StudentRegistrationSerializer


class StudentDetailAPI(BaseAPIViewSet):
    serializer_class = StudentProfileSerializer
    queryset = Student.objects.all()
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
    ]
    obj_user_groups = ["Admin", "Manager", "Accountant"]
    user_lookup_field = "user"
    model_class = Student
    http_method_names = ["get"]
