from random import randrange

from .models import Student, StudentGuardian


def get_profile_status(user):
    student = Student.objects.filter(user=user)

    student_guardian = StudentGuardian.objects.filter(student=student[0])

    guardian_name = student_guardian[0].name
    guardian_phone = student_guardian[0].phone_1

    if None in [
        guardian_name,
        guardian_phone,
    ]:
        return False
    return True


def get_guardian_data(student):
    guardian_data = {
        "guardian_name": "",
        "guardian_phone_1": "",
        "guardian_phone_2": "",
        "guardian_relationship": "",
    }
    guardian = getattr(student, "studentguardian", None)
    if guardian:
        guardian_data.update(
            {
                "guardian_name": student.studentguardian.name,
                "guardian_phone_1": student.studentguardian.phone_1,
                "guardian_phone_2": student.studentguardian.phone_2,
                "guardian_relationship": f"{student.studentguardian.relationship}",
            }
        )
    return guardian_data


def student_no_generator():
    return randrange(1, 10000)
