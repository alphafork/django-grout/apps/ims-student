from django.apps import AppConfig


class ImsStudentConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_student"

    model_strings = {
        "STUDENT": "Student",
        "STUDENT_GUARDIAN": "StudentGuardian",
    }
