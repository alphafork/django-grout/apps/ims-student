from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import GetStudentProfileStatusAPI, StudentDetailAPI, StudentRegistrationAPI

router = routers.SimpleRouter()
router.register(r"details", StudentDetailAPI, "student-detail")

urlpatterns = [
    path("profile-status/", GetStudentProfileStatusAPI.as_view()),
    path("register/", StudentRegistrationAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
