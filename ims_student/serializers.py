from django.contrib.auth.models import Group
from ims_base.serializers import BaseModelSerializer
from ims_user.serializers import UserRegistrationSerializer
from ims_user.services import get_contact_data, get_profile_data
from reusable_models import get_model_from_string

from ims_student.models import Employment, Student, StudentGuardian
from ims_student.services import get_guardian_data, student_no_generator

Contact = get_model_from_string("CONTACT")
Gender = get_model_from_string("GENDER")
Honorific = get_model_from_string("HONORIFIC")
Profile = get_model_from_string("PROFILE")


class StudentRegistrationSerializer(UserRegistrationSerializer):
    def save(self, request):
        user = super().save(request)
        student = Student.objects.create(
            user=user,
            student_no=student_no_generator(),
        )
        StudentGuardian.objects.create(student=student)
        Employment.objects.create(student=student)

        student_group = Group.objects.get(name="Student")
        user.groups.add(student_group)
        return user


class StudentProfileSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = Student

    def to_representation(self, instance):
        return {
            **get_profile_data(instance.user),
            **get_contact_data(instance.user),
            **get_guardian_data(instance),
        }
