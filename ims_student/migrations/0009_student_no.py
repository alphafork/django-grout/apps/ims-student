# Generated by Django 4.1.7 on 2024-05-10 05:07

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("ims_student", "0008_student_placement"),
    ]

    operations = [
        migrations.AddField(
            model_name="student",
            name="student_no",
            field=models.CharField(blank=True, max_length=255, null=True, unique=True),
        ),
    ]
